package granary;

import java.util.*;

public class Shop {
    List<Order> orders = new ArrayList<>();

    public void generateOrders() {

        List<Product> products = Arrays.asList(Product.values()); // lista produktów
        Collections.shuffle(products); //


        for (int i = 0; i < 3 + Math.random() * 10; i++) {
            orders.add(new Order(products.get(i), (int) Math.ceil(Math.random() * 10)));
        }
        // for (Order order : orders) {
        // System.out.println(order);
        //}
    }


    public void printAvailableOrders() {
        generateOrders();
        for (int i = 0; i < orders.size(); i++) {
            System.out.println(i + "\t" + orders.get(i).getProduct() + "  ilość: " + orders.get(i).getQuantity());
        }

    }
}