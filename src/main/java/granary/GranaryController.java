package granary;

import java.util.Scanner;

public class GranaryController extends Shop{

    private Granary granary;

    void begin(){
        granary = new Granary();
        granary.harvest(); // cos na start
        handleCommands();
    }

    void handleCommands(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Co chcesz zrobic?");
        String option = scanner.nextLine();

        switch (option){
            case "harvest":
                granary.harvest();
                System.out.println("Udało sie zebrać plon");
                break;
            case "showProducts":
                System.out.println(granary.showProducts());
                break;
            case "sprzedaj":
                printAvailableOrders();

                System.out.println("Sprzedałeś produkty");
                break;
            default:
                System.out.println("komenda nieprawidłowa");
        }

        handleCommands();

    }
}
