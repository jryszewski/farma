package granary;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class GranaryTest {


    @Test
    public void shouldAddNewProductToGranary() {

        // given
        Granary granary = new Granary();
        granary.addProduct(Product.pomidory, 20);
        granary.addProduct(Product.kapusta, 17);
        granary.addProduct(Product.arbuzy, 2);

        // when
        Map<Product, Integer> products = granary.showProducts();

        // then
        assertEquals(3, products.size());
        assertEquals(20, products.get(Product.pomidory));
        assertEquals(17, products.get(Product.kapusta));
        assertEquals(2, products.get(Product.arbuzy));


    }


    @Test
    public void shouldAddProductsToGranary() {

        // given
        Granary granary = new Granary();
        granary.addProduct(Product.pomidory, 20);
        granary.addProduct(Product.pomidory, 17);
        granary.addProduct(Product.arbuzy, 2);

        // when
        Map<Product, Integer> products = granary.showProducts();

        assertEquals(2, products.size());
        assertEquals(37,products.get(Product.pomidory));
        assertEquals(2,products.get(Product.arbuzy));
    }

    @Test
    public void shouldRemoveProductsFromGranary(){

        Granary granary = new Granary();
        granary.addProduct(Product.pomidory,40);
        granary.removeProduct(Product.pomidory,20);
        granary.addProduct(Product.arbuzy,30);
        granary.removeProduct(Product.arbuzy,30);

        Map<Product, Integer> products = granary.showProducts();
        assertEquals(2,products.size());
        assertEquals(0,products.get(Product.arbuzy));
        assertEquals(20,products.get(Product.pomidory));

    }

    @Test
    public void shouldThrowExceptionWhenTryingToRemoveMoreProductsThanExist(){
        Granary granary = new Granary();
        granary.addProduct(Product.pomidory,40);

        assertThrows(
                RuntimeException.class,
                () -> granary.removeProduct(Product.pomidory,41)
        );

    }
}